@include('templateAdmin.header')

<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('admin/home') }}" class="brand-link">
      <img src="{{asset('images/logo/tab-icon2.png')}}" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SCANDIT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('images/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Dashboard</li>
          <li class="nav-item">
            <a href="{{ url('admin/home') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-line"></i>
              <p class="text">Dashboard</p>
            </a>
          </li>
          <li class="nav-header">Barang</li>
          <li class="nav-item">
            <a href="{{ route('adminBarang') }}" class="nav-link active">
              <i class="nav-icon fas fa-cart-plus"></i>
              <p class="text">Tambah Barang</p>
            </a>
          </li>
          <li class="nav-header">SETTING</li>
          <li class="nav-item">
            <a href="{{ url('/admin/kategori') }}" class="nav-link">
              <i class="nav-icon fas fa-code-branch"></i>
              <p class="text">Kategori Barang</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/pengiriman') }}" class="nav-link">
              <i class="nav-icon fas fa-archive"></i>
              <p>Jasa Pengiriman</p>
            </a>
          </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Detail Barang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('adminBarang') }}">Barang</a></li>
              <li class="breadcrumb-item active">{{$barang->nama_barang}}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-6">
            <div class="card">
              <div class="card-body">
                <div>
                  <h5 class="h5">Nama Barang</h5>
                  <p>{{  $barang->nama_barang }}</p>
                </div>
                <hr>
                <div>
                  <h5 class="h5">Kategori Barang</h5>
                  <p>{{$barang->nama_kategori}}</p>
                </div>
                <hr>
                <div>
                  <h5 class="h5">Harga Satuan</h5>
                  <p>Rp.{{$barang->harga_satuan}}</p>
                </div>
                <hr>
                <div>
                  <h5 class="h5">Berat</h5>
                  <p>{{ $barang->berat }} Kg</p>
                </div>
                <hr>
                <div>
                  <h5 class="h5">Stock</h5>
                  <p>{{ $barang->stock }} Buah</p>
                </div>
                <hr>
                <div>
                  <h5 class="h5">Pembelian Minimum</h5>
                  <p>{{ $barang->pembelian_minimum }} Buah</p>
                </div>
                <hr>
                <div>
                  <h5 class="h5">Kondisi Barang</h5>
                  <p>{{ $barang->kondisi_barang }}</p>
                </div>
                <hr>
                <div>
                  <h5 class="h5">Deskripsi Barang</h5>
                  <p>{{ $barang->deskripsi_barang }}</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6">
            <div class="card">
              <div class="card-body">
                <div class="card-title">Foto Barang</div>
                <hr>
                <div class="row">
                  @foreach ($fotoBarang as $item)
                  <div class="card col-4">
                    <div class="card-body">
                      <img src="{{ asset("foto_barang/$item->id_barang/$item->foto") }}" class="card-img-top" alt="Foto Barang {{ $barang->nama_barang }}">
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

@include('templateAdmin.footer')
