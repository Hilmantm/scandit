@include('templateAdmin.header')

<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('adminHome') }}" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('admin/home') }}" class="brand-link">
      <img src="{{ asset('images/logo/tab-icon2.png') }}" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SCANDIT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('images/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Dashboard</li>
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-line"></i>
              <p class="text">Dashboard</p>
            </a>
          </li>
          <li class="nav-header">Barang</li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-cart-plus"></i>
              <p class="text">Tambah Barang</p>
            </a>
          </li>
          <li class="nav-header">SETTING</li>
          <li class="nav-item">
            <a href="{{ url('/admin/kategori') }}" class="nav-link">
              <i class="nav-icon fas fa-code-branch"></i>
              <p class="text">Kategori Barang</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/pengiriman') }}" class="nav-link">
              <i class="nav-icon fas fa-archive"></i>
              <p>Jasa Pengiriman</p>
            </a>
          </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Barang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Barang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Barang</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-condensed">
                  <tr>
                    <th style="width: 10px">No</th>
                    {{-- <th class="text-center">ID Barang</th> --}}
                    <th class="text-center">Nama Barang</th>
                    <th class="text-center">Kategori Barang</th>
                    <th class="text-center">Stock</th>
                    <th class="text-center" style="width: 200px">Action</th>
                  </tr>
                  @if (!empty($barang))
                    <?php $i = 1; ?>
                    @foreach ($barang as $item)    
                    <tr>
                      <td class="text-center">{{ $i }}</td>
                      {{-- <td class="text-center">{{ $item->id_barang }}</td> --}}
                      {{-- <td class="text-center" style="overflow: hidden">INVjhRuQVcQpzLNve4z90YaZLj50nmf1re1D3lNr5klfS7Sn2D7tVXs8LyAkQ4Tn5thyMBfomkCZmBYdHuVWnNKKLG7GzDw44B8Bq39oCTFcwWtjD9mhOxHtFniv8koPXRi</td> --}}
                      <td class="text-center">{{ $item->nama_barang }}</td>
                      <td class="text-center">{{ $item->nama_kategori }}</td>
                      <td class="text-center">{{ $item->stock }}</td>
                      <td class="text-center">
                        <ul class="list-inline">
                          <li class="list-inline-item">
                            <a href="{{ "barang/" . $item->id_barang . "/edit" }}" class="btn"><i class="fas fa-edit"></i></a>
                          </li>
                          <li class="list-inline-item">
                              <form action="{{ url('admin/barang/' . $item->id_barang ) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                              </form>
                          </li>
                          <li class="list-inline-item">
                            <a href="{{ "barang/$item->id_barang/detail" }}" class="btn"><i class="fas fa-info"></i></a>
                          </li>
                        </ul>
                      </td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                  @endif
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-12">
            <a href="{{ route('adminTambahBarang') }}" class="btn btn-primary float-right">
              <i class="fas fa-plus-circle"></i> Tambah
            </a>
          </div>
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

@include('templateAdmin.footer')
