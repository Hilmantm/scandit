@include('templateAdmin.header')

<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('admin/home') }}" class="brand-link">
      <img src="{{ asset('./images/logo/tab-icon2.png') }}" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SCANDIT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('./images/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Dashboard</li>
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-line"></i>
              <p class="text">Dashboard</p>
            </a>
          </li>
          <li class="nav-header">Barang</li>
          <li class="nav-item">
            <a href="{{ route('adminBarang') }}" class="nav-link">
              <i class="nav-icon fas fa-cart-plus"></i>
              <p class="text">Tambah Barang</p>
            </a>
          </li>
          <li class="nav-header">SETTING</li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-code-branch"></i>
              <p class="text">Kategori Barang</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/pengiriman') }}" class="nav-link">
              <i class="nav-icon fas fa-archive"></i>
              <p>Jasa Pengiriman</p>
            </a>
          </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Kategori Barang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Kategori Barang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Perhatian!!!</strong><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
             </div>
            @endif
          <div class="col12 col-sm-6">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Kategori Barang</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-condensed">
                  <tr>
                    <th style="width: 10px">No</th>
                    <th class="text-center">Kategori Barang</th>
                    <th class="text-center">Action</th>
                  </tr>
                  <?php $i = 1; ?>
                  @foreach ($kategori as $item)    
                    <tr>
                      <td class="text-center">{{ $i }}</td>
                      <td class="text-center">{{ $item->nama_kategori }}</td>
                      <td class="text-center">
                        <ul class="list-inline">
                          @if ($item->nama_kategori != "Accessories" && $item->nama_kategori != "Shoes" && $item->nama_kategori != "Dress")
                            <li class="list-inline-item">
                              <button type="button" class="btn btn-primary" data-kategori="{{ $item->nama_kategori }}" data-link="{{ url('admin/kategori') . "/$item->id_kategori/edit" }}" data-toggle="modal" data-target="#exampleModal" >
                                Edit
                              </button>
                            </li>
                            <li class="list-inline-item">
                              <form action="{{ url('admin/kategori/' . $item->id_kategori) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                              </form>
                            </li>
                            @endif
                        </ul>
                      </td>
                    </tr>
                    <?php $i++; ?>
                  @endforeach
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6">
            <div class="card card-primary">
              <div class="card-header">
                <div class="card-title">Tambah Kategori</div>
              </div>
              <form method="POST" action="{{ route('adminTambahKategoriBarang') }}">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="tambahKategori">Kategori Barang</label>
                    <input type="text" class="form-control" id="tambahKategori" placeholder="Ketik Kategori Baru" name="nama_kategori">
                  </div>
                  <button class="btn btn-primary" type="submit"><i class="fas fa-plus-circle"></i> &nbsp; Tambahkan</button>
                </div>
              </form>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6">
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Jasa Pengiriman</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form id="form" method="POST" action="">
                    @csrf
                    <div class="modal-body">
                      <div class="card-body">
                        <div class="form-group">
                          <label for="tambahKategori">Kategori Barang</label>
                          <input type="text" name="nama_kategori" autocomplete="off" class="form-control" id="tambahKategori" placeholder="Ketik Kategori Baru">
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer> 
</div>
<!-- ./wrapper -->


@include('templateAdmin.footer')

<script>
  $('#exampleModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var kategori = button.data('kategori') // Extract info from data-* attributes
    var link = button.data('link')
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    console.log(kategori)
    modal.find('.modal-title').text('Edit Kategori ' + kategori)
    modal.find('#form').attr('action', link)
    modal.find('.modal-body input').val(kategori)
  })
</script>
