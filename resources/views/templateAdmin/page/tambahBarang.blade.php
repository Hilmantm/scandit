@include('templateAdmin.header')

<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('admin/home') }}" class="brand-link">
      <img src="{{asset('images/logo/tab-icon2.png')}}" alt="SCANDIT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SCANDIT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('images/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ $user->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Dashboard</li>
          <li class="nav-item">
            <a href="{{ route('adminHome') }}" class="nav-link">
              <i class="nav-icon fas fa-chart-line"></i>
              <p class="text">Dashboard</p>
            </a>
          </li>
          <li class="nav-header">Barang</li>
          <li class="nav-item">
            <a href="{{ route('adminBarang') }}" class="nav-link active">
              <i class="nav-icon fas fa-cart-plus"></i>
              <p class="text">Tambah Barang</p>
            </a>
          </li>
          <li class="nav-header">SETTING</li>
          <li class="nav-item">
            <a href="{{ url('/admin/kategori') }}" class="nav-link">
              <i class="nav-icon fas fa-code-branch"></i>
              <p class="text">Kategori Barang</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/pengiriman') }}" class="nav-link">
              <i class="nav-icon fas fa-archive"></i>
              <p>Jasa Pengiriman</p>
            </a>
          </li>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="{{ route('adminLogout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p class="text">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            @if (!empty($barang))
              <h1 class="m-0 text-dark">Edit Barang</h1>
            @else
              <h1 class="m-0 text-dark">Tambah Barang</h1>
            @endif
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('adminBarang') }}">Barang</a></li>
              @if (!empty($barang))
                <li class="breadcrumb-item active">Edit Barang</li>
              @else
                <li class="breadcrumb-item active">Tambah Barang</li>
              @endif
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Perhatian!!!</strong><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
             </div>
            @endif
          <div class="col-12 col-sm-12">
            <form action="{{ url('/admin/barang', @$barang->id_barang) }}" method="POST" enctype="multipart/form-data">
              @csrf
              @if (!empty($barang))
                  @method('PATCH')
                @else
                  @method('POST')
              @endif
              <div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="namaBarang">Nama Barang</label>
                        <input type="text" class="form-control" id="namaBarang" placeholder="Nama Barang" name="nama_barang" value="{{ old('nama_barang', @$barang->nama_barang) }}">
                      </div>
                      <div class="form-group">
                          <label for="exampleFormControlSelect1">Kategori barang</label>
                          <select class="form-control" id="exampleFormControlSelect1" name="kategori_barang">
                            @foreach ($kategori as $item)
                              <option value="{{$item->id_kategori}}" {{ (old('nama_kategori', @$barang->id_kategori) == $item->id_kategori) ? "selected" : "" }}>{{$item->nama_kategori}}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="">Harga Per Satuan</label>
                          <input type="number" class="form-control" placeholder="Harga Satuan" name="harga_satuan" value="{{ old('harga_satuan', @$barang->harga_satuan) }}">
                      </div>
                      <div class="form-group">
                        <label for="">Berat</label>
                        <div class="input-group mb-3">
                          <input type="number" class="form-control" name="berat" value="{{ old('berat', @$barang->berat) }}">
                          <div class="input-group-append">
                            <span class="input-group-text">Kg</span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="">Stock</label>
                        <div class="input-group mb-3">
                          <input type="number" class="form-control" name="stock" value="{{ old('stock', @$barang->stock) }}">
                          <div class="input-group-append">
                            <span class="input-group-text">Buah</span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="">Pembelian Minimum</label>
                        <div class="input-group mb-3">
                          <input type="number" class="form-control" name="pembelian_minimum" value="{{ old('pembelian_minimum', @$barang->pembelian_minimum) }}">
                          <div class="input-group-append">
                            <span class="input-group-text">Buah</span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="">Kondisi Barang</label>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="kondisi_barang" id="exampleRadios1" value="baru" {{ (old('kondisi_barang', @$barang->kondisi_barang) == "baru") ? "checked" : "" }}>
                          <label class="form-check-label" for="exampleRadios1">
                            Baru
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="kondisi_barang" id="exampleRadios1" value="bekas" {{ (old('kondisi_barang', @$barang->kondisi_barang) == "bekas") ? "checked" : "" }}>
                          <label class="form-check-label" for="exampleRadios1">
                            Bekas
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="">Deskripsi Barang</label>
                        <textarea id="" cols="30" rows="10" class="form-control" name="deskripsi_barang">{{ old('deksripsi_barang', @$barang->deskripsi_barang) }}</textarea>
                      </div>
                      @if (!empty($barang))
                        {{-- <input type="submit" value="simpan"> --}}
                        <button class="btn btn-warning" type="submit"><i class="fas fa-edit"></i> &nbsp; Edit</button>
                      @else
                      {{-- <input type="submit" value="simpan"> --}}
                        <button class="btn btn-primary" type="submit"><i class="fas fa-plus-circle"></i> &nbsp; Tambahkan</button>
                      @endif
                      
                    </div>
                  </div>
                  <div class="col-12 col-sm-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="fotoBarang">Foto Barang</label>
                        <input type="file" multiple class="form-control" id="fotoBarang" placeholder="Nama Barang" name="foto[]" value="{{ old('foto[]', @$barang->foto) }}" onchange="preview()">
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header">
                        Foto Barang
                      </div>
                      <div class="card-body" id="imageUploadSection">
                        {{-- image --}} 
                        @if (!empty($fotoBarang))
                            @foreach ($fotoBarang as $item)
                                <img src="{{ asset("foto_barang/$item->id_barang/$item->foto") }}" class="img-thumbnail col-4 fotoBarang" alt="">
                            @endforeach
                        @endif
                      </div>
                    </div>
                  </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<script>

  let preview = function(event) {

    let imageSection = document.getElementById('imageUploadSection');
    let imageUpload = document.getElementById('fotoBarang');

    let template = function(src) {
      var image = new Image();
      image.classList.add('img-thumbnail');
      image.classList.add('col-4');
      image.classList.add('fotoBarang');
      image.src = src;
      return image;
    }

    let openFile = function(file) {

      let reader = new FileReader();
      reader.onload = function() {
        let dataURL = this.result;
        imageSection.appendChild(template(dataURL));
      };

      reader.readAsDataURL(file);
      
    }


    if(imageUpload.files) {
      imageSection.innerHTML = "";
      [].forEach.call(imageUpload.files, openFile);
      imageUpload.files.forEach(console.log(imageUpload.files));
    }

  }


</script>

@include('templateAdmin.footer')
