@extends('../layouts.app')

@section('content')
<div class="container">
    <form action="{{ url('/admin/setting/' . $admin->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="nama">Nama Lengkap</label>
            <input type="text" class="form-control" id="nama" name="name" value="{{ $admin->name }}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" placeholder="name@example.com" name="email" value="{{ $admin->email }}">
        </div>
        <div class="form-group">
            <label for="fotoProfile">Foto Profile</label>
            <input type="file" class="form-control" id="fotoProfile" name="foto_profile" value="{{ $admin->foto_profile }}">
        </div>
        <div class="form-group">
            <label for="noTelp">No Telp</label>
            <input type="number" class="form-control" id="noTelp" name="no_telp" value="{{ $admin->no_telp }}">
        </div>
        <button type="submit">Submit</button>
    </form>
</div>
@endsection
