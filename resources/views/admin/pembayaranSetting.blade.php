@extends('../layouts.app')

@section('content')
<div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Perhatian!!!</strong><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        @endif
    <form action="{{ url('/admin/setting-pembayaran') . "/" . $admin->id }}" method="post">
        @csrf
        <div class="card">
            <div class="card-body">
                @if (empty($bca))
                    <h3 class="h3">BCA</h3>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="statusBCA" id="check" value="enable" {{ old('statusBCA') ? 'checked' : '' }}>
                        <label class="form-check-label" for="check">
                        Enable
                        </label>
                    </div>
                    <div class="form-group row">
                        <label for="nama_rekening" class="col-sm-2 col-form-label">Nama Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_rekening" placeholder="Nama Rekening" name="nama_rekening_bca" value="{{ old('nama_rekening_bca') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_rekening" class="col-sm-2 col-form-label">No Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="no_rekening" placeholder="No Rekening" name="no_rekening_bca" value="{{ old('no_rekening_bni') }}">
                        </div>
                    </div>   
                @else
                    @foreach ($bca as $bca)
                    <h3 class="h3">BCA</h3>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="statusBCA" id="check" value="enable" {{ (old('statusBCA', @$bca->status) == "enable") ? 'checked' : '' }}>
                        <label class="form-check-label" for="check">
                        Enable
                        </label>
                    </div>
                    <div class="form-group row">
                        <label for="nama_rekening" class="col-sm-2 col-form-label">Nama Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_rekening" placeholder="Nama Rekening" name="nama_rekening_bca" value="{{ old('nama_rekening_bca', @$bca->nama_rekening) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_rekening" class="col-sm-2 col-form-label">No Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="no_rekening" placeholder="No Rekening" name="no_rekening_bca" value="{{ old('no_rekening_bni', @$bca->no_rekening) }}">
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                @if (empty($mandiri))
                    <h3 class="h3">Mandiri</h3>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="statusMandiri" id="check" value="enable" {{ old('statusMandiri') ? 'checked' : '' }}>
                        <label class="form-check-label" for="check">
                        Enable
                        </label>
                    </div>
                    <div class="form-group row">
                        <label for="nama_rekening" class="col-sm-2 col-form-label">Nama Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_rekening" placeholder="Nama Rekening" name="nama_rekening_mandiri" value="{{ old('nama_rekening_mandiri') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_rekening" class="col-sm-2 col-form-label">No Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="no_rekening" placeholder="No Rekening" name="no_rekening_mandiri" value="{{ old('no_rekening_mandiri') }}">
                        </div>
                    </div>
                @else
                    @foreach ($mandiri as $mandiri)
                    <h3 class="h3">Mandiri</h3>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="statusMandiri" id="check" value="enable" {{ (old('statusMandiri', @$mandiri->status) == "enable") ? 'checked' : '' }}>
                        <label class="form-check-label" for="check">
                        Enable
                        </label>
                    </div>
                    <div class="form-group row">
                        <label for="nama_rekening" class="col-sm-2 col-form-label">Nama Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_rekening" placeholder="Nama Rekening" name="nama_rekening_mandiri" value="{{ old('nama_rekening_mandiri', @$mandiri->nama_rekening) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_rekening" class="col-sm-2 col-form-label">No Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="no_rekening" placeholder="No Rekening" name="no_rekening_mandiri" value="{{ old('no_rekening_mandiri', @$mandiri->no_rekening) }}">
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                @if (empty($bni))
                    <h3 class="h3">BNI</h3>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="statusBNI" id="check" value="enable" {{ old('statusBNI') ? 'checked' : '' }}>
                        <label class="form-check-label" for="check">
                        Enable
                        </label>
                    </div>
                    <div class="form-group row">
                        <label for="nama_rekening" class="col-sm-2 col-form-label">Nama Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_rekening" placeholder="Nama Rekening" name="nama_rekening_bni" value="{{ old('nama_rekening_bni') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_rekening" class="col-sm-2 col-form-label">No Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="no_rekening" placeholder="No Rekening" name="no_rekening_bni" value="{{ old('no_rekening_bni') }}">
                        </div>
                    </div>
                @elseif (!empty($bni))
                    @foreach ($bni as $bni)
                    <h3 class="h3">BNI</h3>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="statusBNI" id="check" value="enable" {{ (old('statusBNI', @$bni->status) == "enable") ? 'checked' : '' }}>
                        <label class="form-check-label" for="check">
                        Enable
                        </label>
                    </div>
                    <div class="form-group row">
                        <label for="nama_rekening" class="col-sm-2 col-form-label">Nama Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_rekening" placeholder="Nama Rekening" name="nama_rekening_bni" value="{{ old('nama_rekening_bni', @$bni->nama_rekening) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_rekening" class="col-sm-2 col-form-label">No Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="no_rekening" placeholder="No Rekening" name="no_rekening_bni" value="{{ old('no_rekening_bni', @$bni->no_rekening) }}">
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                @if (empty($btn))
                    <h3 class="h3">BTN</h3>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="statusBTN" id="check" value="enable" {{ old('statusBTN') ? 'checked' : '' }}>
                        <label class="form-check-label" for="check">
                        Enable
                        </label>
                    </div>
                    <div class="form-group row">
                        <label for="nama_rekening" class="col-sm-2 col-form-label">Nama Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_rekening" placeholder="Nama Rekening" name="nama_rekening_btn" value="{{ old('nama_rekening_btn') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_rekening" class="col-sm-2 col-form-label">No Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="no_rekening" placeholder="No Rekening" name="no_rekening_btn" value="{{ old('no_rekening_btn') }}">
                        </div>
                    </div>
                @elseif (!empty($btn))
                    @foreach ($btn as $btn)
                    <h3 class="h3">BTN</h3>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="statusBTN" id="check" value="enable" {{ (old('statusBTN', @$btn->status)) ? 'checked' : '' }}>
                        <label class="form-check-label" for="check">
                        Enable
                        </label>
                    </div>
                    <div class="form-group row">
                        <label for="nama_rekening" class="col-sm-2 col-form-label">Nama Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_rekening" placeholder="Nama Rekening" name="nama_rekening_btn" value="{{ old('nama_rekening_btn', @$btn->nama_rekening) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_rekening" class="col-sm-2 col-form-label">No Rekening</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="no_rekening" placeholder="No Rekening" name="no_rekening_btn" value="{{ old('no_rekening_btn', @$btn->no_rekening) }}">
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
        <button type="submit">Submit</button>
    </form>
</div>
@endsection
