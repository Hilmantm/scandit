<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index');
Route::get('/search', 'UserController@search');
Route::get('/barang/{id}', 'UserController@single');

Auth::routes(['verify' => false]);


// admin route
Route::prefix('admin')->group(function() {

    Route::get('/home', 'AdminController@index')->name('adminHome');
    Route::get('/logout', 'AdminController@logout')->name('adminLogout');
    Route::get('/pengiriman', 'AdminController@pengiriman')->name('adminPengiriman');
    Route::post('/pengiriman', 'AdminController@tambah_jasa_pengiriman_controller')->name('adminTambahJasaPengirimanController');
    Route::get('/barang', 'AdminController@barang')->name('adminBarang');
    Route::get('/barang/tambahBarang', 'AdminController@tambah_barang')->name('adminTambahBarang');
    Route::post('/barang', 'AdminController@tambah_barang_controller')->name('adminTambahBarangController');
    Route::get('/barang/{id}/detail', 'AdminController@detail_barang_controller')->name('adminDetailBarangController');
    Route::get('/barang/{id}/edit', 'AdminController@edit_barang')->name('adminEditBarang');
    Route::patch('/barang/{id}', 'AdminController@edit_barang_controller')->name('adminEditBarangController');
    Route::delete('/barang/{id}', 'AdminController@hapus_barang_controller')->name('adminHapusBarangController');
    Route::delete('/pengiriman/{id}', 'AdminController@hapus_jasa_pengiriman_controller')->name('adminHapusJasaPengirimanController');
    Route::post('/pengiriman/{id}/edit', 'AdminController@edit_jasa_pengiriman_controller')->name('adminEditJasaPengirimanController');
    Route::get('/kategori', 'AdminController@kategori_barang')->name('adminKategoriBarang');
    Route::post('/kategori', 'AdminController@tambah_kategori_barang')->name('adminTambahKategoriBarang');
    Route::delete('/kategori/{id}', 'AdminController@hapus_kategori_barang')->name('adminHapusKategoriBarang');
    Route::post('/kategori/{id}/edit', 'AdminController@edit_kategori_barang')->name('adminEditKategoriBarang');

});
