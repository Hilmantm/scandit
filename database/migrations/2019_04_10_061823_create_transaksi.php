<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->string('kode_transaksi')->unique()->primary();
            $table->date('tanggal');
            $table->integer('pembeli');
            $table->integer('metode_pembayaran');
            $table->integer('jasa_pengiriman');
            $table->string('catatan', 150);
            $table->enum('status', ['diproses', 'dikirim', 'sampai']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
