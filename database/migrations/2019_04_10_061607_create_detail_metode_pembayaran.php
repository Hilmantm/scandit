<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMetodePembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_metode_pembayaran', function (Blueprint $table) {
            $table->bigIncrements('id_detail_pembayaran');
            $table->integer('id_user');
            $table->integer('id_bank');
            $table->string('nama_rekening', 100);
            $table->string('no_rekening', 100);
            $table->enum('status', ['enable', 'disable']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_metode_pembayaran');
    }
}
