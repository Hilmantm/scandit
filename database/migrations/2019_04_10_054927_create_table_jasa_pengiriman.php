<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJasaPengiriman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jasa_pengiriman', function (Blueprint $table) {
            $table->bigIncrements('id_jasa_pengiriman');
            $table->string('nama', 50);
            $table->integer('lama_pengiriman');
            $table->float('harga_per_kilo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jasa_pengiriman');
    }
}
