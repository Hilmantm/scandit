<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = "barang";

    public $fillable = 
    [
        "id_barang",
        "nama_barang", 
        "kategori_barang", 
        "harga_satuan", 
        "berat", 
        "stock", 
        "pembelian_minimum",
        "kondisi_barang",
        "deskripsi_barang",
        "id_penjual"
    ];

    public $primaryKey = "id_barang";

    protected $casts = ['id_barang' => 'string'];

    public $timestamps = true;
}
