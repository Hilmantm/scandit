<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetodePembayaran extends Model
{
    protected $table = "metode_pembayaran";

    public $timestamps = false;

    protected $fillable = ["nama_bank"];

    protected $guarded = ["id_bank"];
}
