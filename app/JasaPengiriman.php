<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JasaPengiriman extends Model
{
    protected $table = "jasa_pengiriman";

    public $timestamps = false;

    public $fillable = ["nama", "lama_pengiriman", "harga_per_kilo"];

    public $primaryKey = "id_jasa_pengiriman";

    public $guarded = ["id_jasa_pengiriman"];
}
