<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoBarang extends Model
{
    protected $table = "foto_barang";

    public $fillable = 
    [
        "id_barang",
        "foto"
    ];

    public $guarded =
    [
        "id_foto_barang"
    ];

    public $primaryKey = "id_foto_barang";

    public $timestamps = false;
}
