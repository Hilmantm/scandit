<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\MetodePembayaran;
use App\DetailMetodePembayaran;
use App\Barang;
use App\JasaPengiriman;
use App\KategoriBarang;
use App\FotoBarang;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Helper\Image;
use App\Http\Controllers\Helper\Pembayaran;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $user = Auth::user();
        $barang = Barang::inRandomOrder()
                    ->join('foto_barang', 'foto_barang.id_barang', '=', 'barang.id_barang')
                    ->join('kategori_barang', 'barang.kategori_barang', '=', 'kategori_barang.id_kategori')
                    ->limit(8)
                    ->groupBy('barang.id_barang')
                    ->get();
        $shoes = Barang::join('kategori_barang', 'kategori_barang.id_kategori', '=', 'barang.kategori_barang')
                    ->join('foto_barang', 'foto_barang.id_barang', '=', 'barang.id_barang')
                    ->where('kategori_barang.nama_kategori', 'Shoes')
                    ->limit(8)
                    ->groupBy('barang.id_barang')
                    ->get();
        $accessories = Barang::join('kategori_barang', 'kategori_barang.id_kategori', '=', 'barang.kategori_barang')
                    ->join('foto_barang', 'foto_barang.id_barang', '=', 'barang.id_barang')
                    ->where('kategori_barang.nama_kategori', 'Accessories')
                    ->limit(8)
                    ->groupBy('barang.id_barang')
                    ->get();
        $dress = Barang::join('kategori_barang', 'kategori_barang.id_kategori', '=', 'barang.kategori_barang')
                    ->join('foto_barang', 'foto_barang.id_barang', '=', 'barang.id_barang')
                    ->where('kategori_barang.nama_kategori', 'Dress')
                    ->limit(8)
                    ->groupBy('barang.id_barang')
                    ->get();
        return view('users.page.index', [
            "barang" => $barang,
            "shoes" => $shoes,
            "accessories" => $accessories,
            "dress" => $dress
        ]);
    }

    public function single($id) {
        $barang = Barang::find($id);
        $foto = FotoBarang::where('id_barang', $id)->get();
        $randomBarang = Barang::inRandomOrder()
                    ->join('foto_barang', 'foto_barang.id_barang', '=', 'barang.id_barang')
                    ->join('kategori_barang', 'barang.kategori_barang', '=', 'kategori_barang.id_kategori')
                    ->limit(4)
                    ->groupBy('barang.id_barang')
                    ->get();
        return view('users.page.single', [
            "barang" => $barang,
            "foto" => $foto,
            "randomBarang" => $randomBarang
        ]);
    }

    public function search(Request $request) {
        $search = $request->search;
        $barang = Barang::where('nama_barang', 'LIKE', "%$search%")
                    ->join('foto_barang', 'foto_barang.id_barang', '=', 'barang.id_barang')
                    ->groupBy('barang.id_barang')
                    ->get();
        $searchValue = $search;
        return view('users.page.search', [
            "barang" => $barang,
            "search" => $searchValue
        ]);
    }

}
