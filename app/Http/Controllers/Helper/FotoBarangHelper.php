<?php

namespace App\Http\Controllers\Helper;

use Illuminate\Support\Facades\Storage;
use App\User;
use App\FotoBarang;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class FotoBarangHelper extends Controller {

    private $foto; 
    private $idBarang;
    private $storagePath;

    public function __construct($foto, $idBarang) {

        $this->foto = $foto;
        $this->idBarang = $idBarang;
        $this->storagePath = public_path() . "/foto_barang";

    }

    private function generate_nama_foto() {

        $result = "";
        do {

            $randomInt = rand(1, 25);
            $randomString = str::random($randomInt);
            $prefix = "NFB";
            $result = $prefix . $randomString;
            $foto = FotoBarang::where('foto', $result)->get();

        } while(!$foto->isEmpty());
        return $result;

    }

    public function store() {

        $randomNamaFoto = $this->generate_nama_foto() . "." . $this->foto->getClientOriginalExtension();

        // 1. Store Ke database dalam bentuk nama foto
        $data = array(
            "id_barang" => $this->idBarang,
            "foto" => $randomNamaFoto 
        );
        FotoBarang::create($data);

        // 2. Store ke local storage
        $specificPath = $this->storagePath . "/" . $this->idBarang;
        $this->foto->move($specificPath, $randomNamaFoto);

    }

    public function update() {

        $fotoBarang = FotoBarang::where('id_barang', $this->idBarang);

        // 1. hapus dari folder public
        for($i = 0; $i < count($fotoBarang->get()); $i++) {
            $foto = $fotoBarang->get()[$i]['foto'];
            unlink($this->storagePath . "/$this->idBarang/$foto");
        }

        // 2. hapus dari database
        $status = FotoBarang::where('id_barang', $this->idBarang)->delete();

        // 3. insert foto baru ke database dan public folder
        for($i = 0; $i < count($this->foto); $i++) {

            $randomNamaFoto = $this->generate_nama_foto() . "." . $this->foto[$i]->getClientOriginalExtension();

            // 3.1. insert foto ke database
            $data = array(
                "id_barang" => $this->idBarang,
                "foto" => $randomNamaFoto 
            );
            FotoBarang::create($data); 

            // 3.2. insert foto ke public folder
            $specificPath = $this->storagePath . "/" . $this->idBarang;
            $this->foto[$i]->move($specificPath, $randomNamaFoto);   

        }

    }

    public function delete() {

        // 1. hapus 
        unlink($storagePath . "/$this->idBarang/$this->foto");

    }

}