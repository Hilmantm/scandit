<?php

namespace App\Http\Controllers\Helper;

use Illuminate\Support\Facades\Storage;
use App\User;
use App\Http\Controllers\Controller;

class Image extends Controller {

    private $requestImage = null;
    private $imageLocation = null;
    private $userId = null;
    private $imageName = null;
    private $fileName = null;

    public function setKategoriGambar($kategori) {

        // check kategori gambar
        if("Profile") {
            $this->imageLocation = storage_path() . "/app/foto_profile/";
        } else if("Barang") {
            $this->imageLocation = storage_path() . "/app/foto_barang/";
        }

    }

    public function setUserId($id) {
        $this->userId = $id;
    }    

    public function getUserId() {
        return $this->userId;
    }

    public function setRequestImage($req) {
        $this->requestImage = $req;
    }

    public function getImageName() {
        return $this->fileName;
    }

    public function updateImage() {

        $user = User::find($this->userId);
        $profile_path = $this->imageLocation . $this->userId;
        $file_name = $user->id . $user->name . time() . "." . $this->requestImage->getClientOriginalExtension();
        $this->fileName = $file_name;
        if($user->foto_profile == null) {
            $this->requestImage->move($profile_path, $file_name);
        } else {
            unlink($profile_path . "/" . $user->foto_profile);
            $this->requestImage->move($profile_path, $file_name); 
        }
        

    }

}