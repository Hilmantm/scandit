<?php

namespace App\Http\Controllers\Helper;

use App\DetailMetodePembayaran;
use App\User;
use App\MetodePembayaran;
use App\Http\Controllers\Controller;

class Pembayaran extends Controller {

    private $namaBank = null;
    private $namaRekening = null;
    private $noRekening = null;
    private $userId = null;
    private $status = null;

    public function __construct($id) {
        $this->userId = $id;
    }

    public function setNamaBank($namaBank) {
        $this->namaBank = $namaBank;
    }

    public function setNamaRekening($namaRekening) {
        $this->namaRekening = $namaRekening;
    }

    public function setNoRekening($noRekening) {
        $this->noRekening = $noRekening;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function storeBank() {
        // $user = User::find($this->userId);
        $input['id_user'] = $this->userId;
        $input['id_bank'] = MetodePembayaran::where('nama_bank', $this->namaBank)->first()->id_bank;
        $input['nama_rekening'] = $this->namaRekening;
        $input['no_rekening'] = $this->noRekening;
        $input['status'] = $this->status;
        return $input;
    }

}