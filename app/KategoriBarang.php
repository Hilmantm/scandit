<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriBarang extends Model
{
    protected $table = "kategori_barang";

    public $timestamps = false;

    public $primaryKey = "id_kategori";

    public $fillable = 
    [
        "nama_kategori"
    ];

    public $guarded = 
    [
        "id_kategori"
    ];
}
