<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailMetodePembayaran extends Model
{
    protected $table = "detail_metode_pembayaran";

    public $timestamps = false;

    public $fillable = ["id_user", "id_bank", "nama_rekening", "no_rekening", "status"];

    public $guarded = ["id_detail_pembayaran"];
}
